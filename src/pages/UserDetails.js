import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function UserDetails(){

    const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

    const [allUser, setAllUser] = useState([]);

    const [userId, setUserId] = useState("");
    const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");

    const [isAdmin, setIsAdmin] = useState(false);

    const [showUpdate, setShowUpdate] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

    const openUpdate = () => setShowUpdate(true); //Will show the modal
	const closeUpdate = () => setShowUpdate(false); //Will hide the modal

    const openEdit = (id) => {
		setUserId(id);

		// Getting a specific user to pass on the edit modal
		fetch(`${process.env.REACT_APP_API_URL}/users/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the user states for editing
			setEmail(data.email);
			setMobileNo(data.mobileNo);
		});

		setShowEdit(true)
	};

    const closeEdit = () => {

		// Clear input fields upon closing the modal
        setEmail('');
        setMobileNo('');

        setShowEdit(false);
	};

        // get all users data
    const fetchUserData = () =>{
        // 	// get all the users from the database
        fetch(`${process.env.REACT_APP_API_URL}/users`, {
            headers:{
            "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllUser(data.map(user => {
            return (
                    <tr key={user._id}>
                        <td>{user._id}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.email}</td>
                        <td>{user.mobileNo}</td>
                        {/* <td>{user.orders}</td> */}
                        {/* {
                            user.map(order) => {
                                return(
                                    <tr key={order._id}>
                                        <td>order._id</td>
                                        <td>order.totalAmount</td>
                                        <td>purchasedOn</td>
                                        <td>productId</td>
                                        <td>productName</td>
                                        <td>quantity</td>

                                    <tr/>
                                )
                            }
                        } */}
                        <td>{user.isAdmin ? "Admin" : "Guest"}</td>
                        <td>
                            {
                                // conditonal rendering on what button should be visible base on the status of the user
                                (user.isAdmin)
                                ?
                                <>
                                    <Button variant="danger" size="sm" onClick={() => makeNonAdmin(user._id, user.email)}>Downgrade to Guest</Button>
                                    <Button variant="danger" size="sm" as={Link} to={`/users/order/${user._id}`}>User's Orders</Button>
                                </>   
                                :
                                    <>
                                        <Button variant="success" size="sm" className="mx-1" onClick={() => makeAdmin(user._id, user.email)}>Promote to Admin</Button>
                                        {/* <Button variant="secondary" size="sm" className="mx-1" onClick={() => openUpdate(user._id)}>Edit</Button> */}
                                        <Button variant="danger" size="sm" as={Link} to={`/users/order/${user._id}`}>User's Orders</Button>
                                    </>

                            }
                    </td>
                </tr>
                )
            }));
        });
        }

useEffect(()=>{
    //fetchData();
    fetchUserData();
}, [])

        // making the user Admin
    const makeAdmin = (id, userEmail) =>{
        console.log(id);
        console.log(userEmail);

        // Using the fetch method to set the isActive property of the product document to false
        fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin/${id}`, {
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Promoted to New Admin!",
                    icon: "success",
                    text: `Successful Admin Promotion.`
                });
                // To show the update with the specific operation initiated.
                fetchUserData();
            }
            else{
                Swal.fire({
                    title: "Error on promoting a guest",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }

        // making a NON admin
    const makeNonAdmin = (id, userEmail) =>{
        console.log(id);
        console.log(userEmail);
    
        // Using the fetch method to set the isActive property of the product document to false
        fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin/${id}`, {
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Downgraded to Guest",
                    icon: "success",
                    text: `Successful on downgrading to Guest.`
                });
                // To show the update with the specific operation initiated.
                fetchUserData();
            }
            else{
                Swal.fire({
                    title: "Error on downgrading a guest",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }

    const updateUser = (event) =>{
        // Prevents page redirection via form submission
        event.preventDefault();
    
        fetch(`${ process.env.REACT_APP_API_URL }/users/update/${userId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                email: email,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
    
            if(data){
                Swal.fire({
                    title: "Successful Update!",
                    icon: "success",
                    text: `User is now updated`
                });
    
                // To automatically add the update in the page
                fetchUserData();
                // Automatically closed the modal
                closeUpdate();
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                });
                closeUpdate();
            }
    
        })
    
        // Clear input fields
        setEmail('');
        setMobileNo('');
    }

return(
        (userRole)
        ?
        <>
        <div id="detailsContainer">
        <div className="mt-5 mb-3 text-center">
            <h1>Admin Dashboard - User Details</h1>
            {/* <Button variant="light" className="
            mx-2" onClick={openUpdate}>User Details</Button> */}
            <Button as={Link} to="/admin" variant="light" className="
            mx-2">Product Details</Button>
        </div>
            <Table id="detailsContent">
                <thead>
                    <tr>
                    <th>User ID</th>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>Email</th>
                    <th>Mobile Number</th>
                    <th>Position</th>
                    <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allUser}
                </tbody>
            </Table> 

            <Modal id='detailsModalContainer' show={showUpdate} fullscreen={true} onHide={closeUpdate}>
            <Form id='detailsModalContent' onSubmit={e => updateUser(e)}>

                <Modal.Header closeButton>
                    <Modal.Title>Update User</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId="email" className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter User Email" 
                            value = {email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="mobilNo" className="mb-3">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Mobile Number" 
                            value = {mobileNo}
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                </Modal.Body>

                <Modal.Footer>
                    { isAdmin 
                        ? 
                        <Button variant="success" type="submit" id="submitBtn">
                            Save
                        </Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn" disabled>
                            Save
                        </Button>
                    }
                    <Button variant="secondary" onClick={closeUpdate}>
                        Close
                    </Button>
                </Modal.Footer>

            </Form>	
        </Modal>
        </div>
        </>
        :
        <Navigate to="/products" />
)

}